#include "cv.h"
#include <cxcore.h>
#include <highgui.h>


int main()
{
    IplImage *image = cvLoadImage(".//lena.jpg");

    cvNamedWindow("Before",CV_WINDOW_AUTOSIZE);
    cvNamedWindow("After",CV_WINDOW_AUTOSIZE);

    IplImage *Gimage = cvCreateImage(cvGetSize(image),IPL_DEPTH_8U,3);

    cvSmooth(image,Gimage,CV_GAUSSIAN,3,3);

    cvShowImage("Before",image);
    cvShowImage("After",Gimage);

    cvReleaseImage(&image);
    cvReleaseImage(&Gimage);

    cvWaitKey(0);

    cvDestroyWindow("Before");
    cvDestroyWindow("After");

    return 0;
}
